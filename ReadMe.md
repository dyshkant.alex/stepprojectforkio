# Список использованных технологий:

1. "del": "^5.1.0",
2. "gulp": "^4.0.2",
3. "gulp-autoprefixer": "^7.0.1",
4. "gulp-clean-css": "^4.3.0",
5. "gulp-file-include": "^2.2.2",
6. "gulp-imagemin": "^7.1.0",
7. "gulp-rename": "^2.0.0",
8. "gulp-sass": "^4.1.0",
9. "gulp-sourcemaps": "^2.6.5",
10. "gulp-ttf2woff": "^1.1.1",
11. "gulp-ttf2woff2": "^3.0.0",
12. "gulp-uglify-es": "^2.0.0",
14. "reset-css": "^5.0.1"
15. "browser-sync": "^2.26.7",
16. "gulp-group-css-media-queries": "^1.2.2",
17. "rename": "^1.0.4"

### Проект выполнил `OleksandrDyshkant`

#### Установка всех необходимых пакетов
```shell
   npm install
```

#### сборка проекта

   ``` shell
      gulp
   ```
 ### структура проекта
   * Исходники находяться в папке #src
   * Проект собираеться в папку Dist
   * js и css файлы подключены с папки Dist
   
   

